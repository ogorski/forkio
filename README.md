# Forkio
Step Project #1 (Forkio) - by Front-end (FE13) students.

Stack: html/scss/gulp/js

github pages : https://sofiv.github.io/forkio/

Leader - Sofia Vynnytska
Co-leader - Oleh Horskyj

Step Project objectives:

Oleh Horskyj:

1. Header (default + responsive)
2. Section - "People Are Talking About Fork"

Sofia Vynnytska:

1. Section "Revolutionary Editor" + buttons (watch/star/fork)
2. Section "Here is what you get"
3. Section "Fork Subscription Pricing"

Both students: Creating gulpfile.js